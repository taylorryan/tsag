package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/taylorryan/tsag/bot"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/nlopes/slack/slackevents"
	"github.com/piquette/finance-go/quote"
	goji "goji.io"
	"goji.io/pat"
)

type Team struct {
	AccessToken string    `json:"access_token"`
	Scope       string    `json:"scope"`
	UserID      string    `json:"user_id"`
	TeamName    string    `json:"team_name"`
	TeamID      string    `json:"team_id"`
	CreatedAt   string    `json:"createdAt"`
	UpdatedAt   string    `json:"updatedAt"`
	Bot         *OauthBot `json:"bot"`
}

// OauthBot data container
type OauthBot struct {
	BotUserID      string `json:"bot_user_id"`
	BotAccessToken string `json:"bot_access_token"`
}

var bots []*bot.Bot

func main() {
	log.Println("tsag bot")

	session, err := mgo.Dial(os.Getenv("MONGODB"))
	if err != nil {
		panic(err)
	}
	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	staticFilesLocation := "/charts"
	fs := http.FileServer(http.Dir(staticFilesLocation))

	mux := goji.NewMux()
	mux.HandleFunc(pat.Get("/oauth"), oauth(session))
	mux.HandleFunc(pat.Get("/health"), healthHandler)
	mux.HandleFunc(pat.Post("/events"), eventsHandler)
	mux.Handle(pat.Get("/charts/*"), http.StripPrefix("/charts/", fs))
	mux.HandleFunc(pat.Get("/"), homeHandler)

	teams := allTeams(session)
	for _, team := range teams {
		createBot(&team)
	}

	http.ListenAndServe("0.0.0.0:5000", mux)
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "<a href=\"https://slack.com/oauth/authorize?scope=bot&client_id=%v\"><img alt=\"Add to Slack\" height=\"40\" width=\"139\" src=\"https://platform.slack-edge.com/img/add_to_slack.png\" srcset=\"https://platform.slack-edge.com/img/add_to_slack.png 1x, https://platform.slack-edge.com/img/add_to_slack@2x.png 2x\" /></a>", os.Getenv("CLIENT_ID"))
}

func healthHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")

	_, err := quote.Get("F")

	if err != nil {
		w.WriteHeader(http.StatusGatewayTimeout)
		fmt.Fprint(w, "not ok")
	} else {
		w.WriteHeader(http.StatusOK)
		fmt.Fprint(w, "ok")
	}
}

func eventsHandler(w http.ResponseWriter, r *http.Request) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	body := buf.String()

	eventsAPIEvent, e := slackevents.ParseEvent(json.RawMessage(body), slackevents.OptionVerifyToken(&slackevents.TokenComparator{VerificationToken: os.Getenv("VERIFY_TOKEN")}))
	if e != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}

	if eventsAPIEvent.Type == slackevents.URLVerification {
		var r *slackevents.ChallengeResponse
		err := json.Unmarshal([]byte(body), &r)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}
		w.Header().Set("Content-Type", "text")
		w.Write([]byte(r.Challenge))
	}
	if eventsAPIEvent.Type == slackevents.CallbackEvent {

		eventTeamID := eventsAPIEvent.TeamID
		for _, bot := range bots {
			if bot.TeamID == eventTeamID {
				bot.EventHandler(eventsAPIEvent)
			}
		}
	}
}

func allTeams(s *mgo.Session) []Team {
	session := s.Copy()
	defer session.Close()

	c := session.DB("tsag").C("teams")

	var teams []Team
	err := c.Find(bson.M{}).All(&teams)
	if err != nil {
		log.Println("Failed get all teams: ", err)
		return nil
	}
	return teams
}

func addTeam(s *mgo.Session, team *Team) *Team {
	session := s.Copy()
	defer session.Close()

	c := session.DB("tsag").C("teams")

	var existingTeam Team
	err := c.Find(bson.M{"teamid": team.TeamID}).One(&existingTeam)
	if err == nil {
		log.Println("Team with this team id already exists: ", existingTeam.TeamID)
		return nil
	}

	err = c.Insert(team)
	if err != nil {
		if mgo.IsDup(err) {
			log.Println("Team with this id already exists: ", err)
			return nil
		}

		log.Println("Failed insert team: ", err)
		return nil
	}

	go createBot(team)

	return team
}

func createBot(team *Team) {
	token := team.Bot.BotAccessToken
	newBot := bot.New(token)
	newBot.User = team.UserID
	newBot.TeamID = team.TeamID

	log.Printf("Starting TSAG for %v", team.TeamName)
	log.Println()

	bots = append(bots, newBot)
}
