module gitlab.com/taylorryan/tsag

require (
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/kr/pretty v0.1.0
	github.com/nlopes/slack v0.6.0
	github.com/piquette/finance-go v1.0.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pplcc/plotext v0.0.0-20180221170324-68ab3c6e05c3
	github.com/shopspring/decimal v0.0.0-20180709203117-cd690d0c9e24 // indirect
	github.com/stretchr/testify v1.5.1
	goji.io v2.0.0+incompatible
	golang.org/x/oauth2 v0.0.0-20181128211412-28207608b838
	golang.org/x/sync v0.0.0-20181108010431-42b317875d0f // indirect
	gonum.org/v1/netlib v0.0.0-20191229114700-bbb4dff026f8 // indirect
	gonum.org/v1/plot v0.0.0-20200111075622-4abb28f724d5
	google.golang.org/appengine v1.3.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)

go 1.13
