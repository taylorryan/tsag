package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/globalsign/mgo"
	"golang.org/x/oauth2"
)

func oauth(s *mgo.Session) func(w http.ResponseWriter, r *http.Request) {
	endpoint := oauth2.Endpoint{
		AuthURL:  "https://slack.com/oauth/v2/authorize",
		TokenURL: "https://slack.com/api/oauth.access",
	}

	return func(w http.ResponseWriter, r *http.Request) {
		session := s.Copy()
		defer session.Close()

		ctx := context.Background()
		conf := &oauth2.Config{
			ClientID:     os.Getenv("CLIENT_ID"),
			ClientSecret: os.Getenv("CLIENT_SECRET"),
			Scopes:       []string{"bot"},
			Endpoint:     endpoint,
		}
		code := r.URL.Query().Get("code")
		tok, err := conf.Exchange(ctx, code)
		if err != nil {
			log.Println(err)
		}

		botData := tok.Extra("bot")

		bot := &OauthBot{
			BotUserID:      botData.(map[string]interface{})["bot_user_id"].(string),
			BotAccessToken: botData.(map[string]interface{})["bot_access_token"].(string),
		}

		team := &Team{
			AccessToken: tok.AccessToken,
			Scope:       tok.Extra("scope").(string),
			UserID:      tok.Extra("user_id").(string),
			TeamName:    tok.Extra("team_name").(string),
			TeamID:      tok.Extra("team_id").(string),
			Bot:         bot,
		}

		addTeam(s, team)

		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusCreated)
		fmt.Fprintln(w, "Successfully added TSAG Bot to Slack.")
	}
}
