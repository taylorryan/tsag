package bot

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAppendIfMissing(t *testing.T) {
	var values []string

	values = appendIfMissing(values, "A")
	assert.Contains(t, values, "A")
	assert.Equal(t, len(values), 1)

	values = appendIfMissing(values, "B")
	assert.Contains(t, values, "A")
	assert.Contains(t, values, "B")
	assert.Equal(t, len(values), 2)

	values = appendIfMissing(values, "B")
	assert.Contains(t, values, "A")
	assert.Contains(t, values, "B")
	assert.Equal(t, len(values), 2)

	values = appendIfMissing(values, "C")
	assert.Contains(t, values, "A")
	assert.Contains(t, values, "B")
	assert.Contains(t, values, "C")
	assert.Equal(t, len(values), 3)
}

func TestHasNumber(t *testing.T) {
	result := hasNumber("F")

	assert.False(t, result, "hasNumber should return false")

	result = hasNumber("3")
	assert.True(t, result, "hasNumber should return true")

	result = hasNumber("8B")
	assert.True(t, result, "hasNumber should return true")
}
