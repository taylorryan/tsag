package bot

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
	"unicode"

	"github.com/nlopes/slack"
	"github.com/nlopes/slack/slackevents"
	"github.com/piquette/finance-go/chart"
	"github.com/piquette/finance-go/datetime"
	"github.com/piquette/finance-go/quote"

	"github.com/pplcc/plotext/custplotter"
	"gonum.org/v1/plot"
)

const (
	space               = " "
	dash                = "-"
	newLine             = "\n"
	invalidToken        = "invalid token"
	helpCommand         = "help"
	directChannelMarker = "D"
	userMentionFormat   = "<@%s>"
	codeMessageFormat   = "`%s`"
	boldMessageFormat   = "*%s*"
	italicMessageFormat = "_%s_"
	slackBotUser        = "USLACKBOT"
)

// TOHLCV Object
type TOHLCV struct {
	T float64
	O float64
	H float64
	L float64
	C float64
	V float64
}

// Bot is the main object
type Bot struct {
	client              *slack.Client
	TeamID              string
	Token               string
	User                string
	ID                  string
	errorHandler        func(err string)
	defaultEventHandler func(interface{})
}

// New creates a new bot
func New(token string) *Bot {
	client := slack.New(token, slack.OptionDebug(true), slack.OptionLog(log.New(os.Stdout, "slack-bot: ", log.Lshortfile|log.LstdFlags)))
	bot := &Bot{
		client: client,
	}
	return bot
}

// EventHandler receives events from Slack and each is handled as needed
func (bot *Bot) EventHandler(event slackevents.EventsAPIEvent) {
	innerEvent := event.InnerEvent

	switch innerEvent.Data.(type) {
	case *slackevents.MessageEvent:
		messageEvent := innerEvent.Data.(*slackevents.MessageEvent)

		if bot.isFromBot(messageEvent) {
			break
		}

		go bot.handleMessage(messageEvent)
	}
}

func (bot *Bot) sendMessage(text string, channel string) {
	bot.client.SendMessage(channel, slack.MsgOptionText(text, false))
}

func (bot *Bot) handleMessage(msg *slackevents.MessageEvent) {
	if strings.ToLower(msg.Text) == "stupid bot" {
		bot.sendMessage(fmt.Sprintf("Stupid <@%v>", msg.User), msg.Channel)
		return
	}

	bot.lookForStockSymbols(msg)
}

func (bot *Bot) isDirectMessage(event *slackevents.MessageEvent) bool {
	return strings.HasPrefix(event.Channel, directChannelMarker)
}

func (bot *Bot) isFromBot(event *slackevents.MessageEvent) bool {
	return len(event.User) == 0 || event.User == slackBotUser || len(event.BotID) > 0
}

func (bot *Bot) lookForStockSymbols(msg *slackevents.MessageEvent) {
	symbols := parseStockSymbols(msg.Text)

	for _, symbol := range symbols {
		if strings.ToLower(symbol) == "restart" {
			bot.sendMessage(":recycle: TSAG is restarting, hold on.", msg.Channel)
			os.Exit(0)
		} else {
			go bot.sendStockResponse(symbol, msg.Channel)
		}
	}
}

func (bot *Bot) getChart(stockSymbol string, channel string, timestamp string) {
	t := time.Now()
	t6months := t.AddDate(0, -6, 0)
	p := &chart.Params{
		Symbol:   stockSymbol,
		Start:    datetime.New(&t6months),
		End:      datetime.New(&t),
		Interval: datetime.OneDay,
	}
	iter := chart.Get(p)

	var data custplotter.TOHLCVs

	// Iterate over results. Will exit upon any error.
	for iter.Next() {
		b := iter.Bar()
		O, _ := b.Open.Float64()
		H, _ := b.High.Float64()
		L, _ := b.Low.Float64()
		C, _ := b.Close.Float64()

		tohlcv := TOHLCV{
			T: float64(b.Timestamp),
			O: O,
			H: H,
			L: L,
			C: C,
			V: float64(b.Volume),
		}

		data = append(data, tohlcv)
	}

	c, err := plot.New()
	if err != nil {
		log.Fatal(err)
	}

	c.Title.Text = stockSymbol
	c.X.Label.Text = "Time"
	c.Y.Label.Text = "Price"
	c.X.Tick.Marker = plot.TimeTicks{Format: "2006-01-02\n15:04:05"}

	bars, err := custplotter.NewOHLCBars(data)
	if err != nil {
		log.Panic(err)
	}

	c.Add(bars)

	filename := fmt.Sprintf("/charts/%s-%d.png", stockSymbol, t.Unix())
	err = c.Save(450, 200, filename)
	if err != nil {
		log.Panic(err)
	}

	params := slack.FileUploadParameters{
		Title:           stockSymbol,
		Filetype:        "png",
		File:            filename,
		ThreadTimestamp: timestamp,
		Channels:        []string{channel},
	}
	_, fileErr := bot.client.UploadFile(params)
	if fileErr != nil {
		fmt.Printf("%s\n", fileErr)
	}

	os.Remove(filename)
}

func (bot *Bot) sendStockResponse(stockSymbol string, channel string) {
	q, err1 := quote.Get(stockSymbol)

	if err1 != nil || q == nil {
		if err1 != nil {
			log.Println("sendStockResponse")
			log.Println(err1)
		}

		bot.sendMessage(fmt.Sprintf("%v does not look like a valid stock symbol", stockSymbol), channel)
		return
	} else if q.RegularMarketPrice == 0 {
		bot.sendMessage(fmt.Sprintf("%v does not look like a valid stock symbol, stock price is $0", stockSymbol), channel)
		return
	}

	var stockIcon string
	if q.RegularMarketChange < 0 {
		stockIcon = ":chart_with_downwards_trend:"
	} else {
		stockIcon = ":chart_with_upwards_trend:"
	}

	// Header Section
	header := fmt.Sprintf("%s *<https://finance.yahoo.com/quote/%s|%s (%s)>*", stockIcon, q.Symbol, q.Symbol, q.ShortName)
	headerText := slack.NewTextBlockObject("mrkdwn", header, false, false)

	// Data Section
	var blocks []*slack.TextBlockObject

	lastTradeText := slack.NewTextBlockObject("mrkdwn", fmt.Sprintf("*Last Trade:*\n$%.2f", q.RegularMarketPrice), false, false)
	changeText := slack.NewTextBlockObject("mrkdwn", fmt.Sprintf("*Change:*\n$%.2f (%.2f%%)", q.RegularMarketChange, q.RegularMarketChangePercent), false, false)
	rangeText := slack.NewTextBlockObject("mrkdwn", fmt.Sprintf("*52 Week Range:*\n$%.2f - $%.2f", q.FiftyTwoWeekLow, q.FiftyTwoWeekHigh), false, false)

	blocks = append(blocks, lastTradeText)
	blocks = append(blocks, changeText)
	blocks = append(blocks, rangeText)

	extendedHours := false
	var extHoursText *slack.TextBlockObject
	if q.MarketState == "PREPRE" || q.MarketState == "PRE" {
		extendedHours = true
		extHoursText = slack.NewTextBlockObject("mrkdwn", fmt.Sprintf("*Pre Market:*\n$%.2f (%.2f%%)", q.PreMarketChange, q.PreMarketChangePercent), false, false)
	} else if q.MarketState == "POST" || q.MarketState == "POSTPOST" {
		extendedHours = true
		extHoursText = slack.NewTextBlockObject("mrkdwn", fmt.Sprintf("*Post Market:*\n$%.2f (%.2f%%)", q.PostMarketChange, q.PostMarketChangePercent), false, false)
	}

	if extendedHours {
		blocks = append(blocks, extHoursText)
	}

	dataSection := slack.NewSectionBlock(headerText, blocks, nil)

	_, timestamp, _, err := bot.client.SendMessage(channel, slack.MsgOptionBlocks(dataSection))
	if err != nil {
		log.Println("sendStockResponse - send error")
		log.Printf("Errr %v", err)
		bot.sendMessage(fmt.Sprintf("Error send message for %v, please try again later.", stockSymbol), channel)
		return
	}

	go bot.getChart(stockSymbol, channel, timestamp)
}

func parseStockSymbols(message string) []string {
	re := regexp.MustCompile(`\r?\n`)
	singleLine := re.ReplaceAllString(message, " ")

	words := strings.Split(singleLine, " ")

	var symbols []string

	for _, word := range words {
		if strings.HasPrefix(word, "$") {
			symbol := strings.TrimPrefix(word, "$")

			var replacer = strings.NewReplacer("!", "", ",", "", "@", "", "#", "", "$", "", "%", "", "&", "", "(", "", ")", "", "?", "", "<", "", ">", "")
			symbol = replacer.Replace(symbol)

			_, err := strconv.ParseFloat(symbol, 64)
			if err != nil && symbol != "" && !hasNumber(symbol) {
				symbols = appendIfMissing(symbols, symbol)
			}
		}
	}

	return symbols
}

func appendIfMissing(slice []string, i string) []string {
	for _, ele := range slice {
		if ele == i {
			return slice
		}
	}
	return append(slice, i)
}

func hasNumber(s string) bool {
	for _, r := range s {
		if unicode.IsDigit(r) {
			return true
		}
	}
	return false
}
