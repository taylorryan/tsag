# Accept the Go version for the image to be set as a build argument.
# Default to Go 1.11
ARG GO_VERSION=1.13

# First stage: build the executable.
FROM golang:${GO_VERSION}-alpine AS builder

# Install the Certificate-Authority certificates for the app to be able to make
# calls to HTTPS endpoints.
# Git is required for fetching the dependencies.
RUN apk add --no-cache ca-certificates git

# Set the working directory outside $GOPATH to enable the support for modules.
WORKDIR /src

# Fetch dependencies first; they are less susceptible to change on every build
# and will therefore be cached for speeding up the next build
COPY ./go.mod ./go.sum ./
RUN go mod download

# Import the code from the context.
COPY ./ ./

RUN mkdir /charts

# Build the executable to `/app`. Mark the build as statically linked.
RUN CGO_ENABLED=0 go build \
  -installsuffix 'static' \
  -o /app .

RUN CGO_ENABLED=0 \
  GOOS=linux \
  GOARCH=amd64 \
  go build -a -tags netgo -o /healthcheck healthcheck/healthcheck.go 

# Get timezone info
FROM alpine:latest as tz
RUN apk --no-cache add tzdata zip ca-certificates
WORKDIR /usr/share/zoneinfo
# -0 means no compression.  Needed because go's
# tz loader doesn't handle compressed data.
RUN zip -r -0 /zoneinfo.zip .

# Final stage: the running container.
FROM scratch AS final

# Import the Certificate-Authority certificates for enabling HTTPS.
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

# Import the compiled executables from the first stage.
COPY --from=builder /app /app
COPY --from=builder /healthcheck /healthcheck

COPY --from=builder /charts /charts

# Health Check
HEALTHCHECK --interval=1s --timeout=1s --start-period=2s --retries=3 CMD [ "/healthcheck" ]

# the timezone data:
ENV ZONEINFO /zoneinfo.zip
COPY --from=tz /zoneinfo.zip /

# Declare the port on which the webserver will be exposed.
# As we're going to run the executable as an unprivileged user, we can't bind
# to ports below 1024.
EXPOSE 5000

# Environment Variables
ENV TOKEN ''
ENV PORT 5000

# Run the compiled binary.
ENTRYPOINT ["/app"]